#!/bin/bash

args="$@"

args="$@ -p 80"

file=./db.json
if [ -f $file ]; then
    echo "Found db.json, trying to open"
    args="$args db.json"
fi

file=./file.js
if [ -f $file ]; then
    echo "Found file.js seed file, trying to open"
    args="$args file.js"
fi

json-server $args

# json-server --watch db.json --port 80
# npm run start
# npm run run:prod