FROM node:12.9

RUN npm install -g json-server
WORKDIR /data

COPY . /data

EXPOSE 80

ENTRYPOINT ["/bin/bash", "/data/run.sh"]
